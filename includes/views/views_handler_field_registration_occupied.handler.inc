<?php
/**
 * @file
 * Views handler for the registration occupied field.
 */
class views_handler_field_registration_occupied extends views_handler_field_numeric {
  function query() {
    $table_alias = $this->ensure_my_table();

    // Force group by
    $group = 0;
    if (isset($this->options['group'])) {
      $group = $this->options['group'];
    }
    $this->query->add_having_expression($group, 'TRUE', array());

    $join = new views_join();
    $join->construct('registration', $this->table_alias, 'entity_id', 'entity_id', array(), 'LEFT');
    $alias = $this->query->ensure_table('registration_count', NULL, $join);

    // Add the field.
    $params = $this->options['group_type'] != 'group' ? array('function' => $this->options['group_type']) : array();
    $field = "IFNULL(SUM($alias.count), 0)";
    $this->field_alias = $this->query->add_field(FALSE, $field, 'occupied', $params);

    $this->add_additional_fields();
  }
}
