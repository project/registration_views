<?php
/**
 * @file
 * Views handler for the registration available field.
 */
class views_handler_field_registration_available extends views_handler_field_numeric {
  function query() {
    $this->ensure_my_table();

    // Force group by
    $group = 0;
    if (isset($this->options['group'])) {
      $group = $this->options['group'];
    }
    $this->query->add_having_expression($group, 'TRUE', array());

    $join = new views_join();
    $join->construct('registration', $this->table_alias, 'entity_id', 'entity_id', array(), 'LEFT');
    $alias = $this->query->ensure_table('registration_count', NULL, $join);

    // Add the field.
    $params = $this->options['group_type'] != 'group' ? array('function' => $this->options['group_type']) : array();
    $field = "(IF(IFNULL($this->table_alias.capacity, 0) = 0, 9999, $this->table_alias.capacity) - IFNULL(SUM($alias.count), 0))";
    $this->field_alias = $this->query->add_field(FALSE, $field, 'available', $params);

    $this->add_additional_fields();
  }
}
