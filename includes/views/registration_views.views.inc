<?php

/**
 * @file
 * Views hooks implementations.
 */
function registration_views_views_data_alter(&$data) {
  $data['registration_entity']['occupied'] = array(
    'title' => t('Occupied'),
    'real field' => 'entity_id',
    'help' => t('The number of occupied slots for this node'),
    'field' => array(
      'handler' => 'views_handler_field_registration_occupied',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_field_registration_occupied',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_field_registration_occupied',
    ),
  );
  $data['registration_entity']['available'] = array(
    'title' => t('Available'),
    'real field' => 'entity_id',
    'help' => t('The number of available slots for this node (9999 if this registration has no limit)'),
    'field' => array(
      'handler' => 'views_handler_field_registration_available',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_field_registration_available',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_field_registration_available',
    ),
  );
}
